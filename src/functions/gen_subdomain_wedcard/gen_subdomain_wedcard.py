import os
import zlib
import json
import random
import base64
import logging
from urllib.parse import urljoin
from urllib.error import HTTPError
from urllib.request import urlopen

CONTENT = open('index.html', 'r', encoding='utf8').read()
RESERVED_SUBDOMAINS = ['api', 'static', 'dev', 'www']
API_ENDPOINT = urljoin('https://api.newlywed.me', 'latest/')

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def error_resp(code=503):
    err_map = {
        403: 'Forbidden',
        404: 'Not Found',
        503: 'Internal Server Error',
    }
    body = '%d %s' % (code, err_map[code])
    comp = zlib.compressobj(wbits=31)
    body = base64.b64encode(
                comp.compress(body.encode('utf-8')) + comp.flush()
            )
    return {
        'headers': {
            'content-type': [{
                'key': 'Content-Type',
                'value': 'text/html; charset=utf-8'
            }],
            'content-encoding': [{
                'key': 'Content-Encoding',
                'value': 'gzip'
            }],
            'x-lambda-version': [{
                'key': 'X-Lambda-Version',
                'value': '20'
            }]
        },
        'body': body,
        'bodyEncoding': 'base64',
        'status': str(code),
        'statusDescription': err_map[code]
    }
    
def lambda_handler(evt, ctx):
    logger.info('evt: %s' % str(evt))
    logger.info('ctx: %s' % repr(ctx.__dict__))

    subdomain = None
    origin_request = evt['Records'][0]['cf']['request']
    host = origin_request['headers']['host'][0]['value'].split('.')

    #: can't provide ssl protection
    if len(host) > 3: return error_resp(404)
    if len(host) == 3: subdomain = host[0]

    if subdomain in RESERVED_SUBDOMAINS: return error_resp(404)
    else:
        api_url = urljoin(API_ENDPOINT, 'wedCard/%s' % subdomain)
        try:
            wd_pf = json.loads(
                urlopen(api_url).read().decode()
            )
        except HTTPError as e:
            error_resp_body = e.read()
            error_resp_body = json.loads(error_resp_body.decode()) if \
                                error_resp_body is not None else \
                                error_resp_body
            logger.error(
                'api call GET %s %s %s' % \
                (api_url, e.code, error_resp_body.get('message'))
            )
            return error_resp(e.code)
        except Exception as e:
            logger.exception(e)
            return error_resp(503)

        if not wd_pf: return error_resp(404)

        title        = wd_pf['title']
        url          = 'https://%s' % '.'.join(host)
        main_img_url = wd_pf.get('main_photo')['url'] \
                        if wd_pf.get('main_photo') is not None \
                        else random.choice(wd_pf['gallery_photos'])['url']
        desc         = '%s & %s의 결혼식에 초대합니다' % (
                            wd_pf['contacts']['groom']['name'],
                            wd_pf['contacts']['bride']['name']
                        )

        body = CONTENT \
                .replace('%%MAIN_IMAGE%%', main_img_url) \
                .replace('%%TITLE%%', title) \
                .replace('%%URL%%', url) \
                .replace('%%DESCRIPTION%%', desc) \
                .replace('%%REDUX_STATE%%', json.dumps(wd_pf).replace('\\', '\\\\').replace('"', '\\"'))

        #: gzip
        comp = zlib.compressobj(wbits=31)
        body = base64.b64encode(
                    comp.compress(body.encode('utf-8')) + comp.flush()
                ) \
                .decode('utf-8')
        resp = {
            'headers': {
                'content-type': [{
                    'key': 'Content-Type',
                    'value': 'text/html; charset=utf-8'
                }],
                'content-encoding': [{
                    'key': 'Content-Encoding',
                    'value': 'gzip'
                }],
                'x-lambda-version': [{
                    'key': 'X-Lambda-Version',
                    'value': '20'
                }]
            },
            'body': body,
            'bodyEncoding': 'base64',
            'status': '200',
            'statusDescription': 'OK'
        }
        return resp
