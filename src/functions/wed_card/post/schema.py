import fastjsonschema
from datetime import datetime

validate_wed_profile = fastjsonschema.compile({
    'type': 'object',
    'required': ['tag', 'userphone', 'useremail', 'profile', 'username', 'password'],
    'properties': {
        'tag': {'type': 'string', 'pattern': r'^[a-z-]{1,50}$'},
        'userphone': {'type': 'string', 'format': 'phonenumber'},
        'useremail': {'type': 'string', 'format': 'email'},
        'username': {'type': 'string', 'minLength': 2, 'maxLength': 20},
        'password': {'type': 'string', 'minLength': 4, 'maxLength': 20},
        'profile': {
            'type': 'object',
            'required': ['title', 'message', 'gallery_photos', 'contacts', 'loc', 'time'],
            'properties': {
                'title': {'type': 'string', 'minLength': 3},
                'message': {'type': 'string', 'maxLength': 1000},
                'main_photo': {
                    'type': 'object',
                    'properties': {
                        'filetype': {'type': 'string', 'format': 'allowedFileType'},
                        'width': {'type': 'number'},
                        'height': {'type': 'number'}
                    },
                    'required': ['filetype']
                },
                'gallery_photos': {
                    'type': 'array',
                    'items': {
                        'type': 'object',
                        'properties': {
                            'filetype': {'type': 'string', 'format': 'allowedFileType'},
                            'width': {'type': 'number'},
                            'height': {'type': 'number'}
                        },
                        'required': ['filetype']
                    },
                    'maxItems': 12
                },
                'contacts': {
                    'type': 'object',
                    'required': ['bride', 'groom'],
                    'properties': {
                        'bride': {
                            'type': 'object',
                            'required': ['name'],
                            'properties': {
                                'name': {'type': 'string', 'minLength': 2},
                                'phone_number': {'type': 'string', 'format': 'phonenumber'},
                            }
                        },
                        'groom': {
                            'type': 'object',
                            'required': ['name'],
                            'properties': {
                                'name': {'type': 'string', 'minLength': 2, 'maxLength': 20},
                                'phone_number': {'type': 'string', 'format': 'phonenumber'},
                            }
                        },
                        'father_of_bride': {
                            'type': 'object',
                            'required': ['name'],
                            'properties': {
                                'name': {'type': 'string', 'minLength': 2, 'maxLength': 20},
                                'phone_number': {'type': 'string', 'format': 'phonenumber'},
                            }
                        },
                        'mother_of_bride': {
                            'type': 'object',
                            'required': ['name'],
                            'properties': {
                                'name': {'type': 'string', 'minLength': 2, 'maxLength': 20},
                                'phone_number': {'type': 'string', 'format': 'phonenumber'},
                            }
                        },
                        'father_of_groom': {
                            'type': 'object',
                            'required': ['name'],
                            'properties': {
                                'name': {'type': 'string', 'minLength': 2, 'maxLength': 20},
                                'phone_number': {'type': 'string', 'format': 'phonenumber'},
                            }
                        },
                        'mother_of_groom': {
                            'type': 'object',
                            'required': ['name'],
                            'properties': {
                                'name': {'type': 'string', 'minLength': 2, 'maxLength': 20},
                                'phone_number': {'type': 'string', 'format': 'phonenumber'},
                            }
                        }
                    }
                },
                'loc': {
                    'type': 'object',
                    'required': ['name', 'lat', 'lng', 'detail', 'access'],
                    'properties': {
                        'name': {'type': 'string', 'minLength': 3, 'maxLength': 150},
                        'access': {
                            'type': 'object',
                            'properties': {
                                'by_bus': {'type': 'string', 'maxLength': 700},
                                'by_car': {'type': 'string', 'maxLength': 700},
                                'by_metro': {'type': 'string', 'maxLength': 700}
                            }
                        },
                        'detail': {'type': 'string', 'minLength':3, 'maxLength': 200},
                        'lat': {'type': 'number', 'maximum': 180, 'minimum': 0},
                        'lng': {'type': 'number', 'maximum': 180, 'minimum': 0},
                        'address': {'type': 'string', 'maxLength': 2000},
                    }
                },
                'time': {
                    'type': 'number',
                    'maximum': datetime(year=2100, month=12, day=31).timestamp(),
                    'minimum': datetime(year=2000, month=1, day=1).timestamp()
                },
            }
        }
    }
},
formats={
    'phonenumber': r'^[0-9]{10,11}$|^[0-9]{2,3}-[0-9]{3,4}-[0-9]{4}$',
    'email': r'^\S+@\S+\.\S+$',
    'allowedFileType': r'^image/jp(e)?g$|^image/png$|^image/bmp$'
})
