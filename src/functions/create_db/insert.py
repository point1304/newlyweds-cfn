import boto3

db = boto3.resource('dynamodb')
tb = db.Table('nwd_wed_profile')
tb.put_item(
    Item={
        'id': 'ecks',
        'created': 
    }
)
{
        title: '우 리 결 혼 합 니 다',
        message: '소중하고 귀한 짝을 만났습니다.\n이제 부부라는 이름으로\n인생의 앞날을 함께 채워가고자 합니다.\n\n둘이 되어 시작하는 삶의 첫 장,\n귀한 걸음으로 축복해주시면\n마음에 정히 새기고 살아겠습니다.',
        galleryPhotos: [
            'https://www.blogilates.com/wp-content/uploads/2018/10/thekiss.jpg',
            'https://wallpapermemory.com/uploads/237/lion-wallpaper-hd-2560x1440-255626.jpg',
            'https://g-grafolio.pstatic.net/20191114_210/1573702007733OQVi5_JPEG/%C3%BB%C3%B8%C0%E5%B1%D7%B8%B2.jpg?type=w896_2',
            'https://g-grafolio.pstatic.net/20191023_178/1571812723755p3zOF_PNG/%C3%B5%B8%B6%C3%D1%B1%DD%B0%FC.png?type=w896_2',
            'https://www.blogilates.com/wp-content/uploads/2018/10/thekiss.jpg',
            'https://wallpapermemory.com/uploads/237/lion-wallpaper-hd-2560x1440-255626.jpg',
            'https://g-grafolio.pstatic.net/20191114_210/1573702007733OQVi5_JPEG/%C3%BB%C3%B8%C0%E5%B1%D7%B8%B2.jpg?type=w896_2',
            'https://g-grafolio.pstatic.net/20191023_178/1571812723755p3zOF_PNG/%C3%B5%B8%B6%C3%D1%B1%DD%B0%FC.png?type=w896_2',
            'https://wallpapermemory.com/uploads/237/lion-wallpaper-hd-2560x1440-255626.jpg',
            'https://g-grafolio.pstatic.net/20191114_210/1573702007733OQVi5_JPEG/%C3%BB%C3%B8%C0%E5%B1%D7%B8%B2.jpg?type=w896_2',
            'https://g-grafolio.pstatic.net/20191023_178/1571812723755p3zOF_PNG/%C3%B5%B8%B6%C3%D1%B1%DD%B0%FC.png?type=w896_2',
            'https://www.blogilates.com/wp-content/uploads/2018/10/thekiss.jpg',
        ],
        mainPhoto: 'https://www.blogilates.com/wp-content/uploads/2018/10/thekiss.jpg',
        contacts: {
            bride: {
                name: '원은채',
                phoneNumber: '0106888596',
                email: 'timefiles@gmail.com',
                kakao: 'kakao://x-url-callback.bla'
            },
            groom: {
                name: '임경수',
                phoneNumber: '01031921304',
                email: 'point1304@gmail.com',
            },
            motherOfBride: {
                name: '서결자',
                phoneNumber: '01011112222',
                email: null,
            },
            fatherOfBride: {
                name: '원해연',
                phoneNumber: '010929191919',
                email: null,
            },
            motherOfGroom: {
                name: '배순옥',
                phoneNumber: '01066620904',
                email: 'soonok@hanmail.net',
            },
            fatherOfGroom: {
                name: '임헌영',
                phoneNumber: '01062054484',
                email: 'hako936@naver.com',
            },
        },
        weddingInfo: {
            loc: {
                name: '노블발렌티 대치점',
                lat: 37.29300914,
                lng: 28.10939000,
            },
            time: 1582333200000,
            detailedLoc: '노블발렌티 대치점 1F 은채홀'
        }
    }