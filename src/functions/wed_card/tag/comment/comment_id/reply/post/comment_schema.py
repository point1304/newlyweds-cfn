import fastjsonschema

validate_comment = fastjsonschema.compile({
    'type': 'object',
    'required': ['tag', 'text', 'password', 'name', 'parent'],
    'properties': {
        'tag': {'type': 'string', 'pattern': r'[a-z-]{1,50}'},
        'text': {'type': 'string', 'minLength': 1, 'maxLength': 1000},
        'password': {'type': 'string', 'minLength': 4, 'maxLength': 20},
        'name': {'type': 'string', 'minLength': 2, 'maxLength': 30},
        'parent': {'type': 'integer', 'minimum': 1 << 21, 'exclusiveMaximum': 1 << 63},
    }
})