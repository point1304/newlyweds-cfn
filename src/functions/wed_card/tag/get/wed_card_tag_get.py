import os
import boto3
import logging
import json_extended as json

logger = logging.getLogger('fetch_wed_profile')
logger.setLevel(logging.INFO)

ddb = boto3.resource('dynamodb')
tb  = ddb.Table(os.environ['DDB_WED_CARD']) # pylint: disable=no-member

def lambda_handler(event, context):
    logger.info('evt: %s' % repr(event))

    wd_pf = tb.get_item(Key={'tag': event['pathParameters']['tag']}).get('Item', {}).get('profile', [])
    return {
        'statusCode': '200' if wd_pf is not None else 404,
        'body': json.dumps(wd_pf),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token',
            'Access-Control-Allow-Methods': 'DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT'
        }
    }
