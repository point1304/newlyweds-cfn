import re
import os
import json
import boto3
import random
import decimal
import logging
import fastjsonschema
from datetime import datetime, timezone
from comment_schema import validate_comment
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key, Attr
from werkzeug.security import generate_password_hash

CUSTOM_EPOCH = datetime(year=2019, month=1, day=1, tzinfo=timezone.utc).timestamp()

ddb = boto3.resource('dynamodb')
cmt_tb  = ddb.Table(os.environ['DDB_COMMENT']) # pylint: disable=no-member
wdpf_tb  = ddb.Table(os.environ['DDB_WED_CARD']) # pylint: disable=no-member
cmt_ct_tb = ddb.Table(os.environ['DDB_COMMENT_COUNTER']) # pylint: disable=no-member

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def get_id(isreply=False):
    global CUSTOM_EPOCH
    elapsed_ms = round((datetime.now().timestamp() - CUSTOM_EPOCH) * 1000)
    id_ = elapsed_ms << (62 - 41)
    id_ |= random.randrange(0, 2**21)
    if isreply: id_ |= 1 << 63 
    return id_

def get_created_ts(id_, isreply=False):
    global CUSTOM_EPOCH
    if isreply: id_ = id_ - (1 << 63)
    elapsed_ms = id_ >> 21
    return CUSTOM_EPOCH + (elapsed_ms / 1000)

def json_resp(status_code, body):
    return {
        'statusCode': status_code,
        'body': json.dumps(body),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token',
            'Access-Control-Allow-Methods': 'DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT'
        }
    }

def secure_string(s):
    #: cache the compiled pattern as a global variable
    global __JSON_SECRET_PTN__
    try: __JSON_SECRET_PTN__
    except: __JSON_SECRET_PTN__ = re.compile(r'\"(password|pw)\":\s?\".*?\"')
    return __JSON_SECRET_PTN__.sub('"\\1":"[SECURED]"', s)

def lambda_handler(evt, ctx):
    """POST `/wedCard/{tag}/comment`"""
    logger.info('evt: %s' % secure_string(repr(evt)))

    tag = evt['pathParameters']['tag']

    if evt['httpMethod'] == 'GET':
        result = cmt_tb.query(
            KeyConditionExpression=Key('tag').eq(tag) & Key('id').lt(1 << 63),
            ProjectionExpression='id,#NAME,ip,#TEXT,parent',
            ExpressionAttributeNames={'#TEXT': 'text', '#NAME': 'name'},
        )
        count = int(
            cmt_ct_tb.get_item(Key={'tag': tag}).get('Item', {}).get('counter')
        )

        #: modify data
        for el in result['Items']:
            children_result = cmt_tb.query(
                IndexName='parent_idx',
                KeyConditionExpression=Key('tag').eq(tag) & Key('parent').eq(el['id']),
                ProjectionExpression='id,#NAME,ip,#TEXT,parent',
                ExpressionAttributeNames={'#TEXT': 'text', '#NAME': 'name'}
            )
            
            for child in children_result['Items']:
                child.update({
                    'ts': get_created_ts(int(child['id']), isreply=True),
                    'id': str(child['id']),
                    'parent': str(child['parent'])
                })

            children = {
                'end_id': children_result.get('LastEvaluatedKey', None),
                'data': children_result['Items']
            }

            el.update({
                'children': children,
                'ts': get_created_ts(int(el['id'])),
                'id': str(el['id'])
            })

        ret = {
            'end_id': result.get('LastEvaluatedKey', None),
            'count': count,
            'data': result['Items']
        }
        logger.info('GET /wedCard/%s/comment :: success :: 200' % tag)
        return json_resp('200', ret)

    elif evt['httpMethod'] == 'POST':
        #: Form validation.
        data = json.loads(evt['body'], parse_float=decimal.Decimal)    
        try:
            data.update({'tag': tag})
            data = validate_comment(data)
        except fastjsonschema.JsonSchemaException as e:
            logging.exception(e)
            return json_resp('400', {'error': 'InvalidFormError', 'code': '400', 'message': e.message})

        #: From updated before thrown to ddb.
        data.update({
            'id': get_id(),
            'ip': evt['requestContext']['identity']['sourceIp'],
            'password': generate_password_hash(data['password'], method='pbkdf2:sha256:10'),
        })

        try:
            ddb.meta.client.transact_write_items(
                TransactItems=[
                    {
                        'ConditionCheck': {
                            'TableName': os.environ['DDB_WED_CARD'],
                            'Key': {
                                'tag': tag,
                            },
                            'ConditionExpression': 'attribute_exists(tag)'
                        }
                    },
                    {
                        'Update': {
                            'TableName': os.environ['DDB_COMMENT_COUNTER'],
                            'Key': {
                                'tag': tag,
                            },
                            'UpdateExpression': 'SET #COUNTER = #COUNTER + :count',
                            'ExpressionAttributeValues': {
                                ':count': 1,
                            },
                            'ExpressionAttributeNames': {
                                '#COUNTER': 'counter'
                            },
                        }
                    },
                    {
                        'Put': {
                            'TableName': os.environ['DDB_COMMENT'],
                            'Item': data,
                            #'ConditionExpression': 'attribute_not_exists(id)'
                        }
                    },
                    {
                        'Put': {
                            'TableName': os.environ['DDB_REPLY_COUNTER'],
                            'Item': {
                                'tag': tag,
                                'id': data['id'],
                                'counter': 0
                            }
                        }
                    }
                ]
            )
        except ClientError as e:
            logging.exception(e)
            resp = e.response
            status_code = resp['ResponseMetadata']['HTTPStatusCode']
            return json_resp(
                status_code,
                {
                    'error': resp['Error']['Code'],
                    'code': status_code,
                    'message': resp['Error']['Message']
                }
            )

        logger.info('POST /wedCard/%s/comment :: success :: 201' % tag)


        del data['password'], data['tag']
        data.update({
            'id': str(data['id']),
            'ts': get_created_ts(data['id']),
            'children': {
                'end_id': None,
                'count': 0,
                'data': []
            }
        })

        return json_resp('201', data)