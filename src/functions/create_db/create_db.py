import boto3
import hashlib
from datetime import datetime
from string import ascii_letters
from random import choices, randrange
from decimal import Decimal

"""REQUIREMENTS
1. nw_wed_profile
  1) query and sort by `created_at` timestamp
  2) query by id
  3) query by useremail or phonenumber

  - items
    1. id : S : pk
    2. created : N : 
    4. created_idx_shard : N : GSI(PK)
    5. craeted : N : GSI(SK)
    6. profile : M :
    7. useremail : S
    8. userphone : S : GSI

  - created_idx
    1. created_idx_sd : N : PK
    2. created : N : SK
    3. id : S : NKI

  - userphone_idx
    1. userphone : S : PK
    2. id : S : NKI

2. nw_comment
  1) query by tag
  - items:
    1. 
"""
"""
def dbatt(name, _type):
    return {
        'AttributeName': name,
        'AttributeType': _type
    }
def get_shard_key(s, shards):
    return int(hashlib.sha256(s.encode()).hexdigest(), 16) % shards

db = boto3.resource('dynamodb')
try:
    db.create_table(
        AttributeDefinitions=[
            dbatt('id', 'S'),
            dbatt('created', 'N'),
            dbatt('created_idx_sd', 'N'),
            dbatt('userphone', 'S')
        ],
        TableName='wed_profile',
        KeySchema=[{
            'AttributeName': 'id',
            'KeyType': 'HASH'
        }],
        GlobalSecondaryIndexes=[
            {
                'IndexName': 'created_idx',
                'KeySchema': [
                    {
                        'AttributeName': 'created_idx_sd',
                        'KeyType': 'HASH'
                    },
                    {
                        'AttributeName': 'created',
                        'KeyType': 'RANGE'
                    }
                ],
                'Projection': {
                    'ProjectionType': 'INCLUDE',
                    'NonKeyAttributes': [
                        'id'
                    ]
                }
            },
            {
                'IndexName': 'userphone_idx',
                'KeySchema': [
                    {
                        'AttributeName': 'userphone',
                        'KeyType': 'HASH'
                    }
                ],
                'Projection': {
                    'ProjectionType': 'INCLUDE',
                    'NonKeyAttributes': [
                        'id'
                    ]
                }
            }
        ],
        BillingMode='PAY_PER_REQUEST'
    )
except Exception as e:
    print(e)
    pass

db.create_table(
    TableName='comment',
    AttributeDefinitions=[
        dbatt('id', 'S'),
        dbatt('id_created', 'N')
    ],
    BillingMode='PAY_PER_REQUEST',
    KeySchema=[
        {
            'AttributeName': 'id',
            'KeyType': 'HASH'
        },
        {
            'AttributeName': 'id_created',
            'KeyType': 'RANGE'
        }
    ]
)


tb = db.Table('wed_profile')

for el in range(10):
    _id = ''.join(choices(ascii_letters, k=5))
    created = datetime(year=2019, month=12, day=21)
    profile = {
        'bride': 'eunchae',
        'groom': 'kyeongsu'
    }
    useremail = ''.join(choices(ascii_letters, k=5)) + '@gmail.com'
    userphone = '010' + ''.join(choices('1234567890', k=8))
    tb.put_item(
        Item={
            'id': _id,
            'created': int(created.timestamp()),
            'profile': profile,
            'created_idx_sd': int(hashlib.sha256(_id.encode()).hexdigest(), 16) % 4,
            'useremail': useremail,
            'userphone': userphone
        }
    )

for el in range(15):
    _id = ''.join(choices(ascii_letters, k=5))
    created = datetime(year=2019, month=6, day=11)
    profile = {
        'bride': 'junhee',
        'groom': 'jerry'
    }
    useremail = ''.join(choices(ascii_letters, k=5)) + '@gmail.com'
    userphone = '010' + ''.join(choices('1234567890', k=8))
    tb.put_item(
        Item={
            'id': _id,
            'created': int(created.timestamp()),
            'created_idx_sd': int(hashlib.sha256(_id.encode()).hexdigest(), 16) % 4,
            'profile': profile,
            'useremail': useremail,
            'userphone': userphone
        }
    )
"""
__CUSTOM_EPOCH__ = datetime(year=2019, month=1, day=1).timestamp()

def get_id():
    global __CUSTOM_EPOCH__
    elapsed_ms = round((datetime.now().timestamp() - __CUSTOM_EPOCH__) * 1000)
    _id = elapsed_ms << (64 - 41)
    _id |= randrange(0, 2 ** 23)
    return _id

def get_created(_id):
    global __CUSTOM_EPOCH__
    elapsed_ms = _id >> 23
    ts = __CUSTOM_EPOCH__ + (elapsed_ms / 1000)
    return datetime.fromtimestamp(ts)

db = boto3.resource('dynamodb')
tb = db.Table('comment')

for el in range(10):
    id_ = 'ec'
    created_id = get_id()
    text = ''.join(choices(ascii_letters + ' ', k=20))
    tb.put_item(
        Item={
            'id': id_,
            'id_created': created_id,
            'text': text,
        }
    )

for el in range(15):
    tb.put_item(
        Item={
            'id': 'ks',
            'id_created': get_id(),
            'text': ''.join(choices(ascii_letters, k =20))
        }
    )