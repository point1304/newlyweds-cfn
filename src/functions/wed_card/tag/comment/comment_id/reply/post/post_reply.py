import re
import os
import json
import boto3
import random
import decimal
import logging
import fastjsonschema
from datetime import datetime, timezone
from comment_schema import validate_comment
from botocore.exceptions import ClientError
from werkzeug.security import generate_password_hash

CUSTOM_EPOCH = datetime(year=2019, month=1, day=1, tzinfo=timezone.utc).timestamp()

ddb = boto3.resource('dynamodb')

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def get_id(isreply=False):
    global CUSTOM_EPOCH
    elapsed_ms = round((datetime.now().timestamp() - CUSTOM_EPOCH) * 1000)
    id_ = elapsed_ms << (62 - 41)
    id_ |= random.randrange(0, 2**21)
    if isreply: id_ |= 1 << 63 
    return id_

def get_created_ts(id_, isreply=False):
    global CUSTOM_EPOCH
    if isreply: id_ = id_ - (1 << 63)
    elapsed_ms = id_ >> 21
    return CUSTOM_EPOCH + (elapsed_ms / 1000)

def json_resp(status_code, body):
    return {
        'statusCode': status_code,
        'body': json.dumps(body),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token',
            'Access-Control-Allow-Methods': 'DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT'
        }
    }

def secure_string(s):
    #: cache the compiled pattern as a global variable
    global __JSON_SECRET_PTN__
    try: __JSON_SECRET_PTN__
    except: __JSON_SECRET_PTN__ = re.compile(r'\"(password|pw)\":\s?\".*?\"')
    return __JSON_SECRET_PTN__.sub('"\\1":"[SECURED]"', s)

def lambda_handler(evt, ctx):
    """POST `/wedCard/{tag}/comment/{comment_id}/reply`"""
    logger.info('evt: %s' % secure_string(repr(evt)))

    tag = evt['pathParameters']['tag']
    comment_id = int(evt['pathParameters']['comment_id'])

    if evt['httpMethod'] == 'POST':
        #: Form validation.
        data = json.loads(evt['body'], parse_float=decimal.Decimal)    
        try:
            data.update({'tag': tag, 'parent': comment_id})
            data = validate_comment(data)
        except fastjsonschema.JsonSchemaException as e:
            logging.exception(e)
            return json_resp('400', {'error': 'InvalidFormError', 'code': '400', 'message': e.message})

        #: From updated before thrown to ddb.
        data.update({
            'id': get_id(isreply=True),
            'ip': evt['requestContext']['identity']['sourceIp'],
            'password': generate_password_hash(data['password'], method='pbkdf2:sha256:10'),
        })

        try:
            ddb.meta.client.transact_write_items(
                TransactItems=[
                    {
                        'ConditionCheck': {
                            'TableName': os.environ['DDB_COMMENT'],
                            'Key': {
                                'tag': tag,
                                'id': comment_id
                            },
                            'ConditionExpression': 'attribute_exists(id)'
                        }
                    },
                    {
                        'Update': {
                            'TableName': os.environ['DDB_REPLY_COUNTER'],
                            'Key': {
                                'tag': tag,
                                'id': comment_id
                            },
                            'UpdateExpression': 'SET #COUNTER = #COUNTER + :count',
                            'ExpressionAttributeValues': {
                                ':count': 1,
                            },
                            'ExpressionAttributeNames': {
                                '#COUNTER': 'counter'
                            },
                        }
                    },
                    {
                        'Put': {
                            'TableName': os.environ['DDB_COMMENT'],
                            'Item': data,
                            #'ConditionExpression': 'attribute_not_exists(id)'
                        }
                    }
                ]
            )
        except ClientError as e:
            logging.exception(e)
            resp = e.response
            status_code = resp['ResponseMetadata']['HTTPStatusCode']
            return json_resp(
                status_code,
                {
                    'error': resp['Error']['Code'],
                    'code': status_code,
                    'message': resp['Error']['Message']
                }
            )

        logger.info('POST /wedCard/%s/comment :: success :: 201' % tag)


        del data['password'], data['tag']
        data.update({
            'id': str(data['id']),
            'ts': get_created_ts(data['id'], isreply=True),
            'parent': str(data['parent'])
        })

        return json_resp('201', data)