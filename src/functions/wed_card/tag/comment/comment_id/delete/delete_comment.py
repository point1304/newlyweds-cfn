import re
import os
import json
import boto3
import logging
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key, Attr
from werkzeug.security import check_password_hash

ddb = boto3.resource('dynamodb')
cmt_tb  = ddb.Table(os.environ['DDB_COMMENT']) # pylint: disable=no-member

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def json_resp(status_code, body):
    return {
        'statusCode': status_code,
        'body': json.dumps(body),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token',
            'Access-Control-Allow-Methods': 'DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT'
        }
    }

def isreply(id_):
    return (int(id_) >> 63) == 1

def secure_string(s):
    #: cache the compiled pattern as a global variable
    global __JSON_SECRET_PTN__
    try: __JSON_SECRET_PTN__
    except: __JSON_SECRET_PTN__ = re.compile(r'\"(password|pw)\":\s?\".*?\"')
    return __JSON_SECRET_PTN__.sub('"\\1":"[SECURED]"', s)

def lambda_handler(evt, ctx):
    """DELETE `/wedCard/{tag}/comment/{comment_id}`"""
    logger.info('evt: %s' % secure_string(repr(evt)))

    tag = evt['pathParameters']['tag']
    comment_id = int(evt['pathParameters']['comment_id'])
    password = json.loads(evt.get('body', '{}')).get('password')

    if password is None:
        return json_resp(
            '400',
            {
                'error': 'InvalidFormError',
                'code': '400',
                'message': 'no password input'
            }
        )

    item = cmt_tb.get_item(
                Key={'tag': tag, 'id': comment_id},
                ProjectionExpression='password,parent'
            ) \
            .get('Item', {})

    password_hash = item.get('password')
    parent_id = item.get('parent')

    if password_hash is None:
        return json_resp(
            '409',
            {
                'error': 'ResourceConflict',
                'code': '409',
                'message': 'no resource found with given comment id'
            } 
        )

    if not check_password_hash(password_hash, password):
        return json_resp(
            '409',
            {
                'error': 'PasswordNotMatching',
                'code': '409',
                'message': 'password is not matching'
            }
        )
    else:
        try:
            counter_table_name = os.environ['DDB_COMMENT_COUNTER']
            counter_key_obj = {'tag': tag}

            if isreply(comment_id):
                counter_table_name = os.environ['DDB_REPLY_COUNTER']
                counter_key_obj.update({'id': parent_id})

            transaction = [
                {
                    'Delete': {
                        'TableName': os.environ['DDB_COMMENT'],
                        'Key': {
                            'tag': tag,
                            'id': comment_id
                        }
                    }
                },
                {
                    'Update': {
                        'TableName': counter_table_name,
                        'Key': counter_key_obj,
                        'UpdateExpression': 'SET #COUNTER = #COUNTER - :count',
                        'ExpressionAttributeValues': {
                            ':count': 1,
                        },
                        'ExpressionAttributeNames': {
                            '#COUNTER': 'counter'
                        }
                    }
                }
            ]

            if not isreply(comment_id):
                transaction.append({
                    'Delete': {
                        'TableName': os.environ['DDB_REPLY_COUNTER'],
                        'Key': {
                            'tag': tag,
                            'id': comment_id
                        }
                    }
                })
            #: Better off with SNS Topic Message for lambda worker invocation
            #: which will handle the async deleteion logic.
            #if not isreply(comment_id):
            #    transaction.append({
            #        'Put': {
            #            'TableName': os.environ['DDB_DELETED_COMMENT'],
            #            'Item': {
            #                'tag': tag,
            #                'id': comment_id
            #            }
            #        }
            #    })

            ddb.meta.client.transact_write_items(
                TransactItems=transaction
            )
        except ClientError as e:
            logging.exception(e)
            resp = e.response
            status_code = resp['ResponseMetadata']['HTTPStatusCode']
            return json_resp(
                status_code,
                {
                    'error': resp['Error']['Code'],
                    'code': status_code,
                    'message': resp['Error']['Message']
                }
            )

        logger.info('DELETE /wedCard/%s/comment/%s :: success :: 200' % (tag, comment_id))

        return json_resp('201', {'success': True})