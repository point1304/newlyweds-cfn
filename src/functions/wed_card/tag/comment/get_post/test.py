import boto3
from boto3.dynamodb.conditions import Key, Attr

ddb = boto3.resource('dynamodb')

if __name__ == '__main__':
    try:
        ddb.meta.client.transact_write_items(
            TransactItems=[
                {
                    'ConditionCheck': {
                        'TableName': 'nwd_wed_card',
                        'Key': {
                            'tag': 'asdfsdf',
                        },
                        'ConditionExpression': 'attribute_exists(tag)'
                    }
                },
                {
                    'Update': {
                        'TableName': 'nwd_comment_counter',
                        'Key': {
                            'tag': 'love',
                        },
                        'UpdateExpression': 'SET #COUNTER = #COUNTER + :count',
                        'ExpressionAttributeValues': {
                            ':count': 1,
                        },
                        'ExpressionAttributeNames': {
                            '#COUNTER': 'counter'
                        },
                    }
                },
            ]
        )
    except Exception as e:
        print(e.__dict__)