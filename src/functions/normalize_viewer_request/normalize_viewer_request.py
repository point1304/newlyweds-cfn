import logging
from urllib.parse import parse_qs, urlencode

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def lambda_handler(evt, ctx):
    logger.info('evt: %s' % evt)
    req = evt['Records'][0]['cf']['request']

    req['querystring'] = ''
    req['uri'] = '/'

    return req