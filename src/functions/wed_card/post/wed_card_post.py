import os
import re
import decimal
import hashlib
import logging
from datetime import datetime

import boto3
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Attr, Key

import fastjsonschema
import json_extended as json
from werkzeug.security import generate_password_hash

from schema import validate_wed_profile

ddb = boto3.resource('dynamodb')
tb = ddb.Table(os.environ['DDB_WED_CARD']) # pylint: disable=no-member

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

decimal.getcontext().prec = 6

def json_resp(status_code, body):
    return {
        'statusCode': status_code,
        'body': json.dumps(body),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token',
            'Access-Control-Allow-Methods': 'DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT'
        }
    }

def secure_string(s):
    #: cache the compiled pattern as a global variable
    global __JSON_SECRET_PTN__
    try: __JSON_SECRET_PTN__
    except: __JSON_SECRET_PTN__ = re.compile(r'\"(password|pw)\":\s?\".*?\"')
    return __JSON_SECRET_PTN__.sub('"\\1":"[SECURED]"', s)

def get_wedcard_image_url(tag, n, filetype):
    allowed_n = ('m','0','1','2','3','4','5','6','7','8','9','10','11')
    filetype_ext_map = {
        'image/jpg': '.jpg',
        'image/jpeg': '.jpg',
        'image/png': '.png',
    }
    if n not in allowed_n:
        raise ValueError(
            'invalid input for `n`.\n`n` need to be one of %s' \
                % repr(allowed_n)
        )
    if filetype not in filetype_ext_map:
        raise ValueError(
            'invalid input for `filetype`.\n`filetype has to be one of %s' \
                % repr(filetype_ext_map.keys())
        )
    ret = 'https://static.newlywed.me/gallery/{}/{}' \
        .format(
            tag,
            hashlib.sha224((tag + n).encode()).hexdigest() + filetype_ext_map[filetype]
        )
    return ret
    

def lambda_handler(evt, ctx):
    logger.info('evt: %s' % secure_string(repr(evt)))
    data = json.loads(evt['body'], parse_float=decimal.Decimal)

    try:
        #: validate wed profile
        data = validate_wed_profile(data)
    except fastjsonschema.JsonSchemaException as e:
        logging.exception(e)
        return json_resp('400', {'error': 'InvalidFormError', 'code': '400', 'message': e.message})

    #: update data before throwing into ddb.
    data.update({
        'created': decimal.Decimal(datetime.now().timestamp()),
        'created_idx_sd': int(hashlib.sha256(data['tag'].encode()).hexdigest(), 16) % 4,
        'password': generate_password_hash(data['password'], method='pbkdf2:sha256:10')
    })
    if data['profile'].get('main_photo') is not None:
        data['profile']['main_photo'].update({
            'url': get_wedcard_image_url(data['tag'], 'm', data['profile']['main_photo']['filetype'])})
    for i, el in enumerate(data['profile']['gallery_photos']):
        el.update({'url': get_wedcard_image_url(data['tag'], str(i), el['filetype'])})

    try:
        tb.put_item(
            Item=data,
        )
    except ClientError as e:
        logging.exception(e)
        resp = e.response
        status_code = resp['ResponseMetadata']['HTTPStatusCode']
        return json_resp(
            status_code,
            {'error': resp['Error']['Code'], 'code': status_code, 'message': resp['Error']['Message']}
        )

    logging.info('POST /wedCard :: success :: 201')

    return json_resp('201', {'success': True})